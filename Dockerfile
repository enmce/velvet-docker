FROM ubuntu:14.04

MAINTAINER "Viktor Svekolkin" <enmce@yandex.ru>

RUN apt-get update && apt-get install -y \
	wget \
	tar \
	gcc \
	make \
	zlib1g-dev
# Downloading Velvet sources
RUN wget -P /opt https://www.ebi.ac.uk/~zerbino/velvet/velvet_1.2.10.tgz

WORKDIR /opt

RUN tar -xzf velvet_1.2.10.tgz

WORKDIR /opt/velvet_1.2.10

# Make Velvet with OMP flag
RUN make 'OPENMP=1'






	
